import glob
import scipy.io
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt

folder = "datosZScan/TeGePb/28_06_18/"
endFile = "TeGePb(M)28_06_18"
extension = ".mat"
maxFile = folder + "40mWTeGePb(M)28_06_18.mat"
minFile = folder + "01mWTeGePb(M)28_06_18.mat"

files = [f for f in sorted(glob.glob(folder + "*.mat"))]

def normalizeData(positions, data):
    mean = np.mean(data, axis = 0)
    sd = np.std(data, axis = 0)
    finalList = []
    newPositions = []
    contador = 0
    for x in data:
        if (x > mean - 2 * sd) and (x < mean + 2 * sd):
            finalList.append(x)
            newPositions.append(positions[contador])
        contador += 1
    return [newPositions, finalList]
def maxMin(minimum, data, posC):
    data = data[np.logical_not(np.isnan(data))]
    minimum = minimum[np.logical_not(np.isnan(minimum))]
    fullData = []
    cont = 0
    for value in data:
        fullData.append(value-minimum[cont])
        cont += 1

    return normalizeData(posC, fullData)
    
mat = scipy.io.loadmat(maxFile)
posC = np.absolute(mat['thisData']['traces'][0][0][0][0][3])
posC[:] = [x - 60 for x in posC]

minimum = scipy.io.loadmat(minFile)
TANormalMin = minimum['thisData']['traces'][0][0][0][0][13]
TCNormalMin = minimum['thisData']['traces'][0][0][0][0][11]
TDivididaMin = TCNormalMin/TANormalMin

TANormal = mat['thisData']['traces'][0][0][0][0][13]
TCNormal = mat['thisData']['traces'][0][0][0][0][11]
TDividida = TCNormal/TANormal

data = TANormal
dataMin = TANormalMin
fullData = maxMin(dataMin, data, posC)
fig = plt.figure(figsize = (10,10))
plt.scatter(fullData[0], fullData[1], label = "Abierto")
data = TDividida
dataMin = TDivididaMin
fullData = maxMin(TDivididaMin, TDividida, posC)
plt.scatter(fullData[0], fullData[1], label = "Cerrado/Abierto")    
data = TCNormal
dataMin = TCNormalMin
fullData = maxMin(TCNormalMin, TCNormal, posC)
plt.scatter(fullData[0], fullData[1], label = "Cerrado")    
plt.legend(loc = 'best')    
plt.grid(True)
plt.xlabel('$PosiciónZ(mm)$', fontsize = 18)
plt.ylabel('$TransmitanciaNormalizada$', fontsize = 18)    
plt.show()
fig.savefig(folder + "results/comparison.png")

fig = plt.figure(figsize = (10,10))

for file in files:
    mat = scipy.io.loadmat(file)
    posC = np.absolute(mat['thisData']['traces'][0][0][0][0][3])
    posC[:] = [x - 60 for x in posC]
    TANormal = (mat['thisData']['traces'][0][0][0][0][13])
    TCNormal = mat['thisData']['traces'][0][0][0][0][11]
    TDividida = TCNormal/TANormal

    data = TANormal
    #if (file == minFile):
    #plt.scatter(data[0], data[1], label = file.replace(folder, "").replace(endFile, "").replace(extension, ""))
    #else:
    dataMin = TANormalMin
    fullData = maxMin(dataMin, data, posC)
    plt.scatter(fullData[0], fullData[1], label = file.replace(folder, "").replace(endFile, "").replace(extension, ""))
    
plt.legend(loc = 'best')    
plt.grid(True)
plt.xlabel('$PosiciónZ(mm)$', fontsize = 18)
plt.ylabel('$TransmitanciaNormalizada$', fontsize = 18)    
plt.show()
fig.savefig(folder + "results/open.png")

fig = plt.figure(figsize = (10,10))
for file in files:
    mat = scipy.io.loadmat(file)
    posC = np.absolute(mat['thisData']['traces'][0][0][0][0][3])
    posC[:] = [x - 60 for x in posC]
    TANormal = (mat['thisData']['traces'][0][0][0][0][13])
    TCNormal = mat['thisData']['traces'][0][0][0][0][11]
    TDividida = TCNormal/TANormal

    data = TDividida
    dataMin = TDivididaMin
    fullData = maxMin(dataMin, data, posC)
    plt.scatter(fullData[0], fullData[1], label = file.replace(folder, "").replace(endFile, "").replace(extension, ""))

plt.legend(loc = 'best')    
plt.grid(True)
plt.xlabel('$PosiciónZ(mm)$', fontsize = 18)
plt.ylabel('$TransmitanciaNormalizada$', fontsize = 18)    
plt.show()
fig.savefig(folder + "results/closed.png")

fig = plt.figure(figsize = (10,10))
dPVArray = []
for file in files:
    mat = scipy.io.loadmat(file)
    TANormal = (mat['thisData']['traces'][0][0][0][0][13])
    TCNormal = mat['thisData']['traces'][0][0][0][0][11]
    TDividida = TCNormal/TANormal

    data = TDividida
    dataMin = TDivididaMin
    if file != minFile:
        fullData = maxMin(dataMin, data, posC)

    else:
        fullData = normalizeData(posC, data)
    dPV = max(fullData[1])-min(fullData[1])
    dPVArray.append(dPV)
    plt.scatter(file.replace(folder, "").replace(endFile, "").replace(extension, ""), dPV, label = file.replace(folder, "").replace(endFile, "").replace(extension, ""))    

def fun_obj1(b, x, y):
    return np.sum((y-b[0]-b[1]*x)**2)

b0 = np.random.random((2,))
x = np.linspace(0, 1, len(files))
res = opt.minimize(fun_obj1, b0, args=(x,dPVArray))
yhat = np.array([x**j for j in range(2)]).T.dot(res.x)
files[:] = [file.replace(folder, "").replace(endFile, "").replace(extension, "") for file in files]
plt.plot(files, yhat, '-r', label = 'ajuste')    
plt.legend(loc = 'best')    
plt.grid(True)
plt.xlabel('$Potencia$', fontsize = 18)
plt.ylabel('$dVP$', fontsize = 18)    
plt.show()
fig.savefig(folder + "results/potencia.png")


# =============================================================================
# dtype=[
#        ('det0C', 'O'),0
#        ('det1C', 'O'),1
#        ('TC', 'O'),2
#        ('posC', 'O')3,
#        ('det0A', 'O')4,
#        ('det1A', 'O'),5
#        ('TA', 'O'),6
#        ('posA', 'O')7,
#        ('PA', 'O'),8
#        ('PAerror', 'O'),9
#        ('TC0', 'O'),0
#        ('TCnormal', 'O'),1
#        ('TA0', 'O'),2
#        ('TAnormal', 'O'),3
#        ('T', 'O'),4
#        ('Tnormal', 'O'),5
#        ('time', 'O'),6
#        ('S', 'O'),7
#        ('Pin', 'O'),8
#        ('Ep', 'O'),9
#        ('I0', 'O')])]]0
# =============================================================================
